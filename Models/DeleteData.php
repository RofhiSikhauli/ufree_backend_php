<?php


/**
 * File name: DBQueries.php
 * Description: This file holds DBQueries class methods that generate database queries using method chaining,
 * Purpose: This file made for appName app
 * Date: 13 April 2015
 * Author: Rofhiwa Sikhauli
 * Version v0.0.1
 *
 */


//Class Database extends PDO class
//All the methods within this class are going to return theirselves to make chains
class DeleteData extends UpdateData{

	public function delete($table_name = NULL){

		$table_name = ($table_name === NULL) ? exit("Error: You didn't specify table name") : $table_name;
		$this->query = "DELETE FROM " . $table_name;

		return $this;

	}



}

