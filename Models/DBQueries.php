<?php


/**
 * File name: DBQueries.php
 * Description: This file holds DBQueries class methods that generate database queries using method chaining,
 * Purpose: This file made for appName app
 * Date: 13 April 2015
 * Author: Rofhiwa Sikhauli
 * Version v0.0.1
 */



//class DBQueries is extending Database class
final class DBQueries extends DeleteData{

	private $prepare, $exec;

	//Execute query
	private function execute(array $params = NULL){

		$this->prepare = $this->link->prepare($this->query);

		$this->exec = $this->prepare->execute($params);


	}

	//Query where clause
	public function where($condition = NULL){

		$this->query .= " WHERE ". $condition;

		return $this;

	}


	//fetch one row
	public function fetch(array $params = NULL){

		$this->execute($params);

		return ($this->exec) ? $this->prepare->fetch(PDO::FETCH_ASSOC) : false;

	}


	//fetch all rows
	public function fetch_all(array $params = NULL){

		$this->execute($params);

		return ($this->exec) ? $this->prepare->fetchAll(PDO::FETCH_ASSOC) : false;

	}


	//row count
	public function row_count(array $params = NULL){

		$this->execute($params);

		return ($this->exec) ? $this->prepare->rowCount() : 0;

	}

	//get last inserted id
	public function getLastId(){

		return $this->link->lastInsertId();

	}

	//Run query
	public function run(array $params = NULL){

		$this->execute($params);

		return ($this->exec) ? 1 : 0;
	}


}








