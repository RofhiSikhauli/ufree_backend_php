<?php


/**
 * File name: Database.php
 * Description: This file holds Database class, and two methods construct and connect to connect to database
 * Purpose: This file made for appName app
 * Date: 14 April 2015
 * Author: Rofhiwa Sikhauli
 * Version v0.0.1
 *
 */

class Database extends DBConfig{

	protected $link;

	public function __construct(){

		$this->link = new PDO(parent::DB_TYPE . ':host=' . parent::DB_HOST . ';dbname=' . parent::DB_NAME, parent::DB_USER, parent::DB_PASS); //Connect to the database

	}	

}