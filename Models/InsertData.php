<?php


/**
 * File name: InsertData.php
 * Description: This file holds InsertData class methods that generate database queries using method chaining,
 * Purpose: This file made for appName app
 * Date: 13 April 2015
 * Author: Rofhiwa Sikhauli
 * Version v0.0.1
 *
 */


//Class Database extends PDO class
//All the methods within this class are going to return theirselves to make chains
class InsertData extends SelectData{


	public function insert($table_name = NULL){

		$table_name = ($table_name === NULL) ? exit("Error: You didn't specify table name") : $table_name;
		$this->query = "INSERT INTO " . $table_name;
		return $this;

	}


	public function columns($cols = NULL){

		$cols = ($cols === NULL) ? exit("Error: You didn't specify table columns") : implode(", ", $cols);
		$this->query .= " (" . $cols . ") ";
		return $this;

	}


	public function values($vals = NULL){

		$vals = ($vals === NULL) ? exit("Error: You didn't specify values") : implode(", ", $vals);
		$this->query .= " VALUES (" . $vals . ") ";
		return $this;

	}


}
