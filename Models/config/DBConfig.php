<?php


/**
 * Database configuration
 *
 *
 */

class DBConfig{

	const DB_TYPE = 'mysql';
	const DB_HOST = 'localhost';
	const DB_NAME = 'ufree_db';
	const DB_USER = 'username';
	const DB_PASS = 'password';

}
