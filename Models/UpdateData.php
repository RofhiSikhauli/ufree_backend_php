<?php



/**
 * File name: UpdateData.php
 * Description: This file holds UpdateData class methods that generate database queries using method chaining,
 * Purpose: This file made for appName app
 * Date: 13 April 2015
 * Author: Rofhiwa Sikhauli
 * Version v0.0.1
 *
 */


class UpdateData extends InsertData{


	public function update($table_name = NULL){

		$table_name = ($table_name === NULL) ? exit("Error: You didn't specify table name") : $table_name;
		$this->query = "UPDATE " . $table_name;
		return $this;

	}


	public function set(array $cols = NULL){

		$arr = "";

		foreach ($cols as $key => $value) {

			if($arr === ""){
				$arr = $key . " = " . $value;
			}else{
				$arr .= ", " . $key . " = " . $value;
			}

			
		}


		$this->query .= " SET " . $arr;

		return $this;

	}



}
