<?php


/**
 * File name: SelectData.php
 * Description: This file holds SelectData class methods that generate database queries using method chaining,
 * Purpose: This file made for appName app
 * Date: 13 April 2015
 * Author: Rofhiwa Sikhauli
 * Version v0.0.1
 *
 */


//Class Database extends PDO class
//All the methods within this class are going to return theirselves to make chains
class SelectData extends Database{

	protected $query;

	//Select columns
	public function select($cols = NULL){

		$cols = ($cols === NULL) ? "*" : implode(", ", $cols);
		$this->query = "SELECT " . $cols;

		return $this;

	}

	//from which table
	public function from($table_name = NULL){

		$table_name = ($table_name === NULL) ? exit("Error: You didn't specify table name") : $table_name;
		$this->query .= " FROM ". $table_name;

		return $this;

	}

	//Group by
	public function group_by($group_by = NULL){

		$this->query .= " GROUP BY ". $group_by;

		return $this;

	}


	//Order by
	public function order_by($order_by = NULL){

		$this->query .= " ORDER BY ". $order_by;

		return $this;

	}


	//Inner join
	public function inner_join($inner_join = NULL){

		$this->query .= " INNER JOIN ". $inner_join;

		return $this;

	}

	//left join
	public function left_join($left_join = NULL){

		$this->query .= " LEFT JOIN ". $left_join;

		return $this;

	}

	//right join
	public function right_join($right_join = NULL){

		$this->query .= " RIGHT JOIN ". $right_join;

		return $this;

	}

	//outer join
	public function outer_join($outer_join = NULL){

		$this->query .= " OUTER JOIN ". $outer_join;

		return $this;

	}


	//Where to join tables
	public function join_on($join_on = NULL){

		$this->query .= " ON ". $join_on;

		return $this;

	}

	//Select limit
	public function limit($start, $finish){

		$this->query .= " LIMIT ". $start . ", " . $finish;

		return $this;

	}


}
