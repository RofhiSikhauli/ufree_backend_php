<?php

//Enabling Cross domain request
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With");

date_default_timezone_set('Africa/Johannesburg'); //Set time

//Auto load classes
function __autoload($class_name){

	$folders = array(
			"Models/", 
			"Models/config/",
			"Controllers/",
		);

	foreach ($folders as $folder) {
		
		$filename = $folder . $class_name . ".php";

		if(file_exists($filename)){
			include($filename);
		}
	}
}


$obj = new Response(); //Response class
// $obj = new PostsController(); //Response class

// //Display Response
echo $obj->displayResponse();

