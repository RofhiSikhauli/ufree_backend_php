<?php


final class PostsController{

	private $link, $auth;

	public function __construct(){

		$this->link = new DBQueries();

	}
	

	//load Posts from table
	public function loadPosts($post_type = null, $user_id, $start, $limit, $post_id = null){

		$post_type = ($post_type == null) ? 0 : $post_type;

		$cols = array("Id", "UserId", "Post", "likes_count", "dislike_count", "reply_count", "timestamp");
		
		if($post_type == 0){ //Your posts

			$stmt = $this->link->select($cols)->from("posts")->order_by("Id DESC")->limit($start, $limit)->fetch_all();
		
		}else if($post_type == 1){ //updated posts

			$stmt = $this->link->select($cols)->from("posts")->where("Id = :post_id")->fetch(array(":post_id" => $post_id));

		}

	 	return $stmt;

	}


	// Save user posts into the table
	public function savePosts($user_id, $post, $coordinates, $seen_by){

		$coordinates = ($coordinates === "") ? 0 : $coordinates;
		$timestamp = date("Y-m-d H:i:s");

		$stmt = $this->link->insert("posts")->columns(array("UserId", "Post", "coordinates", "seen_by", "timestamp"))->values(array(":userId", ":post", ":coords", ":seen_by", ":timestamp"))->run(array(":userId" => $user_id, ":post" => $post, ":coords" => $coordinates, ":seen_by" => $seen_by, ":timestamp" => $timestamp));

		if($stmt === 1){
			return $this->link->getLastId();
		}else{
			return $result;
		}

	}


	// Save replies of the posts to the table
	public function replyPosts($user_id , $post_id, $reply_post){

		$result = 0;
		$timestamp = date("Y-m-d H:i:s");

	 	$stmt = $this->link->insert("replies")->columns(array("UserId", "PostId", "Reply", "timestamp"))->values(array(":userId", ":postId", ":reply", ":timestamp"))->run(array(":userId"=>$user_id, ":postId"=>$post_id, ":reply"=>$reply_post, ":timestamp" => $timestamp));
	 	
	 	if($stmt === 1){

	 		$result = $this->incrementReplyCount($post_id);

	 	}

	 	return $result;

	}

	// Select replies based on post id
	public function get_posts_replies($post_id, $start, $limit){

		$cols_reply = array("Reply", "timestamp");

	 	$stmt = $this->link->select($cols_reply)->from("replies")->where("PostId = :post_id")->limit($start, $limit)->fetch_all(array(":post_id" => $post_id));
	 	
	 	return $stmt;

	}


	// Save replies of the posts to the table
	private function incrementReplyCount($post_id){

	 	$stmt = $this->link->update("posts")->set(array("reply_count" => "reply_count + 1"))->where("Id = :postId")->run(array(":postId" => $post_id));

	 	return $stmt;

	}


	// Save likes/dislikes on a likes_dislikes table
	public function likeDislikePosts($user_id, $post_id, $type){

		$validation = $this->validateLikeDislikes($user_id, $post_id); //Get validations
		$result = 0;

		if($validation == "none"){

			if($this->insertLikesDislike($user_id, $post_id, $type) == 1){

				if($type == 0){

					$result = $this->updateLikesDislikeCounter($post_id, array("dislike_count" => "dislike_count + 1"));

				}else if($type == 1){

					$result = $this->updateLikesDislikeCounter($post_id, array("likes_count" => "likes_count + 1"));

				}

			}
	 		
	 	}else if($validation == 1){
	 		
	 		if($type == 0){

	 			if($this->updateLikesDislikeCounter($post_id, array("likes_count" => "likes_count - 1")) == 1){

					if($this->updateLikesDislikeCounter($post_id, array("dislike_count" => "dislike_count + 1")) == 1){

						$result = $this->UpdateLikeDislikeType($user_id, $post_id, $type);

					}
				}	 		

	 		}

	 	}else if($validation == 0){

	 		if($type == 1){

	 			if($this->updateLikesDislikeCounter($post_id, array("dislike_count" => "dislike_count - 1")) == 1){

					if($this->updateLikesDislikeCounter($post_id, array("likes_count" => "likes_count + 1")) == 1){

						$result = $this->UpdateLikeDislikeType($user_id, $post_id, $type);

					}
				}	 			
	 		}

	 	}

	 	return $result;

	}



	// Check if user already liked or disliked post
	public function validateLikeDislikes($user_id, $post_id){

		$count_row = $this->link->select(array(count("UserId") . " as counter", "type"))->from("likes_dislikes")->where("UserId = :userid && PostId = :postid")->fetch(array(":userid" => $user_id , ":postid" => $post_id));

	    if($count_row['counter'] >= 1){

	    	return $count_row['type'];

	    }else{

	    	return "none"; //not liked already

	    }

	}
	

	//Insert to likes_dislike table
	private function insertLikesDislike($user_id, $post_id, $type){

		return $this->link->insert("likes_dislikes")->columns(array("UserId", "PostId", "type"))->values(array(":userId", ":postId", ":type"))->run(array(":userId" => $user_id, ":postId" => $post_id, ":type" => $type));

	}


	//Change like to dislike or dislike to like
	private function UpdateLikeDislikeType($user_id, $post_id, $type){

		return $this->link->update("likes_dislikes")->set(array("type" => ":type"))->where("UserId = :userId && PostId = :postId")->run(array(":userId" => $user_id, ":postId" => $post_id, ":type" => $type));

	}

	//increment the likes on the Post table
	private function updateLikesDislikeCounter($post_id, $arr_change_field){
	 	
	 	$stmt = $this->link->update("posts")->set($arr_change_field)->where("Id = :postId")->run(array(":postId" => $post_id));

	 	return $stmt;
	}
	

	//Count likes and dislikes
	public function countLikeDislikes($column, $post_id){
	 	
	 	$stmt = $this->link->select(array($column))->from("posts")->where("Id=:postId")->fetch(array(":postId" => $post_id));
	 	return $stmt[$column];
	}


	//Save notifcation into the table
	public function saveNotifications($user_id, $notification){

		$stmt = $this->link->insert("notifications")->columns(array("user_id", "notification"))->values(array(":userId", ":notif"))->run(array(":userId"=>$user_id, ":notif"=>$notification));
	 	
	 	return $stmt;

	}


	//Select notifications from table
	public function getNotifications($user_id){

		$stmt = $this->link->select()->from("notifications")->where("user_id = :id")->fetch_all(array(":id" => $user_id));
	 	
	 	return $stmt;

	}


	//Delete user notification after being sent to the user
	public function awknowledge_notification($notif_id){

		$stmt = $this->link->delete("notifications")->where("id = :id")->run(array(":id" => $notif_id));
	 	
	 	return $stmt;

	}



	//Update post on facebook
	public function update_post_on_fb_page($post_id, $user_id){

		$dislikes = $this->countLikeDislikes("dislike_count", $post_id);
		$likes = $this->countLikeDislikes("likes_count", $post_id);
		$notification = "Your post have been updated to the facebook page after receiving more that 5 likes";

		if($likes - $dislikes >= 5) {

			$this->saveNotifications($user_id, $notification);

			//update on facebook page here

		}
	}

	//Delete posts
	public function user_delete_posts($post_id, $user_id){

		$delete_where = "Id = :post_id && UserId = :user_id";
		$params = array(':post_id' => $post_id, ':user_id' => $user_id);

		return $this->link->delete("posts")->where($delete_where)->run($params);

	}

	//Modify/Update post
	public function user_edit_posts($post_id, $user_id, $post_updates){

		$set_params = array("Post" => ":post");
		$update_where = "Id = :post_id && UserId = :user_id";
		$params = array(':post_id' => $post_id, ':user_id' => $user_id, ':post' => $post_updates);

		return $this->link->update("posts")->set($set_params)->where($update_where)->run($params);

	}

	//Delete posts if it recieves more thn 5 dislikes
	public function delete_abusing_posts($post_id, $user_id){

		$notification = "Your post have been deleted after receiving more that 5 dislikes, stop abusing appName, your account will be deleted if your continue";

		if($this->countLikeDislikes("dislike_count", $post_id) >= 5){

			if($this->deleteLikesDislikes($post_id) === 1){

			 	if($this->link->delete("users")->where("Id = :id")->run(array(':id' => $post_id)) === 1){

			 		$this->saveNotifications($user_id, $notification);

			 	}
			}
		}

	}

	//Delete posts if it recieves more thn 5 dislikes
	private function deleteLikesDislikes($post_id){

	 	return $this->link->delete("likes_dislikes")->where("PostId = :post_id")->run(array(':post_id' => $post_id));

	}		



	//Search posts from table
	public function searchPosts($post){

		$cols = array("Id", "UserId", "Post", "likes_count", "dislike_count", "reply_count", "timestamp");

		$stmt = $this->link->select($cols)->from("posts")->where("Post like concat('%', :post, '%')")->group_by("Id DESC")->fetch_all(array(':post'=> $post));
		
	 	return $stmt;

	}

	//Calculate the distance between two GPS coordinates
    public function calculate_distance($lat1, $lng1, $lat2, $lng2, $miles = true){

	    $pi80 = M_PI / 180;
	    $lat1 *= $pi80;
	    $lng1 *= $pi80;
	    $lat2 *= $pi80;
	    $lng2 *= $pi80;
	     
	    $r = 6371; // mean radius of Earth in km
	    $dlat = $lat2 - $lat1;
	    $dlng = $lng2 - $lng1;
	    $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlng / 2) * sin($dlng / 2);
	    $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
	    $km = $r * $c;
	     
	    return ($miles ? ($km * 0.621371192) : $km);

    }



}
