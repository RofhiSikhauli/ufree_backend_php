
<?php

final class Authentication{
	
	private $link;

	public function __construct(){

		$this->link = new DBQueries();

	}

	//Set new authentication when app wakeup or start
	public function set($user_id, $appId){

		$time_stamp = date("Y-m-d h:i:s");
		$token = $this->generate_token($appId, $time_stamp);

		//Delete current record
		$this->destroy($user_id); 

		//Insert data
		$results = $this->link->insert("authentication")->columns(array("UserId", "app_id", "token", "time_stamp"))->values(array(":user_id", ":app_id", ":token", ":time_stamp"))->run(array(":user_id" => $user_id, ":app_id" => $appId, ":token" => $token, "time_stamp" => $time_stamp));

		return $results;							

	}


	//GEt the authentication
	public function get($user_id, $appId){
		
		//Get timestamp and token	
		$results = $this->link->select(array("time_stamp", "token"))->from("authentication")->where("UserId = :user")->fetch(array(":user" => $user_id));	
		
		//Generate new token
		$new_token = $this->generate_token($appId, $results['time_stamp']);

		//Validate tokens
		if($new_token === $results['token']){
			return 1;
		}else{
			return 0;
		}

	}


	//generate token authentication
	private function generate_token($appId, $time_stamp){

		return hash('sha512', $appId . $time_stamp);

	}

	//generate token authentication
	public function validate_app_version($app_version){

		$stmt = $this->link->select(array('id'))->from("app_versions")->where("version_name = :version && status = 1")->row_count(array(':version'=> $app_version));
		
	 	return $stmt;

	}



	//Destroy the authentication when app sleep or stop
	public function destroy($user_id){
		
		return $this->link->delete("authentication")->where("UserId = :user")->run(array(":user" => $user_id));
		
	}
	
}
