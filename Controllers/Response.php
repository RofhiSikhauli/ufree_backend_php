<?php

/**
 * File name: Response.php
 * Description: This file holds all the methods that send the response back to the sender
 * Purpose: This file made for appName app
 * Date: 24 April 2015
 * Author: Rofhiwa Sikhauli
 * Version v0.0.2
 *
 */


//Response class extends Requests
class Response extends Requests{
	

	private $response = array(), $auth;

	public function __construct(){

		$this->auth = new Authentication(); //Institiate Authentication class

		parent::__construct();

	}


	//Register request handler
	protected function register_request(){

		$register = $this->obj->create_new_user($this->email, $this->password);

		if($register === 0){

			$this->response['code'] = 402; 
			$this->response['status'] = "Failed to create an account"; 

		}else if($register === 1){

			$this->response['code'] = 200; 
			$this->response['status'] = "Success"; 

		}else if($register === 2){

			$this->response['code'] = 205; 
			$this->response['status'] = "User already exists"; 

		}else{

			$this->response['code'] = 401; 
			$this->response['status'] = "Request failed"; 

		}
	}


	//Login request handler
	protected function login_request(){

		$check_version = $this->auth->validate_app_version($this->app_version);

		if($check_version === 0){

			$this->response['code'] = 500;
			$this->response['status'] = "App version expired";

		}else{


			$login = $this->obj->loginUser($this->email, $this->password);

			$salt = $this->obj->get_salt($this->email);
			$pass = hash('sha512', $this->password . $salt);	

			if($login["Password"] === $pass){

				$this->response['code'] = 200; 
				$this->response['status'] = "Success"; 
				$this->response['user_id'] = $login['Id']; 

				$this->auth->set($login["Id"], $this->app_id); //Create new authentication

			}else{

				$this->response['code'] = 401; 
				$this->response['status'] = "Invalid login creditials"; 

			}
		}
	}


	//Load posts
	protected function load_posts_request(){


		if($this->auth->get($this->user_id, $this->app_id) === 1){

			$posts = $this->obj->loadPosts(0, $this->user_id, $this->sel_start, $this->sel_limit, null);
			$posts_return = array();
			
			if(is_array($posts) && count($posts) > 0){

				foreach ($posts as $post) {

					$posted_time = GetTimeAgo::time_ago($post['timestamp']);
					$post_id = $post['Id'];
					
					$get_flags = $this->obj->validateLikeDislikes($this->user_id, $post_id);

					if($get_flags == "none"){

						$vote_flag = array("like" => 0, "dislike" => 0);
				
					}else if($get_flags == 0){

						$vote_flag = array("like" => 0, "dislike" => 1);

					}else if($get_flags == 1){

						$vote_flag = array("like" => 1, "dislike" => 0);

					}

					$new_array = array(	"post_id" => $post_id,
										"user_id" => $post['UserId'],
										"Post" => stripslashes($post['Post']),
										"likes_count" => $post['likes_count'],
										"dislike_count" => $post['dislike_count'],
										"reply_count" => $post['reply_count'],
										"posted" => $posted_time,
										"flags" => $vote_flag,
										"timestamp" => $post['timestamp']

									);

					$posts_return[] = $new_array;

				}

				$this->response = $posts_return;

			}else{
				$this->response['code'] = 403;
				$this->response['status'] = "No post found";
			}

			

		}else{

			$this->response['code'] = 401;
			$this->response['status'] = "Invalid token";

		}

	}


	//Save posts
	protected function update_post_request(){

		if($this->auth->get($this->user_id, $this->app_id) === 1){

			$save_post = $this->obj->savePosts($this->user_id, $this->post, $this->coordinates, $this->seen_by);

			if($save_post >= 1){
				$timestamp = date("Y-m-d H:i:s");
				$vote_flag = array("like" => 0, "dislike" => 0);
				$posted_time = GetTimeAgo::time_ago($timestamp);

				$new_array = array(	"post_id" => $save_post,
									"user_id" => $this->user_id,
									"Post" => stripslashes($this->post),
									"likes_count" => 0,
									"dislike_count" => 0,
									"reply_count" => 0,
									"posted" => $posted_time,
									"replies" => array(),
									"flags" => $vote_flag,
									"timestamp" => $timestamp
								);	

				$this->response[] = $new_array;	

			}else{

				$this->response['code'] = 401;
				$this->response['status'] = "Failed to update post";

			}

			

		}else{

				$this->response['code'] = 401;
				$this->response['status'] = "Invalid token";			
		}

	}



	//Save posts replies
	protected function post_reply_request(){

		// $notification = "Your post received new reply";
		if($this->auth->get($this->user_id, $this->app_id) === 1){

			$save_reply = $this->obj->replyPosts($this->user_id, $this->post_id, $this->reply_post);

			if($save_reply === 1){

				$get_replied_post = $this->obj->loadPosts(1, $this->user_id, null, null, $this->post_id);
				$post_id = $get_replied_post['Id'];
				// $replies = $this->obj->get_posts_replies($post_id, 0, 3); //Select replies
				$get_flags = $this->obj->validateLikeDislikes($this->user_id, $post_id);

				
				if($get_flags == "none"){
					
					$vote_flag = array("like" => 0, "dislike" => 0);
			
				}else if($get_flags == 0){

					$vote_flag = array("like" => 0, "dislike" => 1);

				}else if($get_flags == 1){

					$vote_flag = array("like" => 1, "dislike" => 0);

				}

				$posted_time = GetTimeAgo::time_ago($get_replied_post['timestamp']);

				$new_array = array(	"post_id" => $post_id,
									"user_id" => $get_replied_post['UserId'],
									"Post" => stripslashes($get_replied_post['Post']),
									"likes_count" => $get_replied_post['likes_count'],
									"dislike_count" => $get_replied_post['dislike_count'],
									"reply_count" => $get_replied_post['reply_count'],
									"posted" => $posted_time,
									// "replies" => $replies,
									"flags" => $vote_flag
								);	

				$this->response[] = $new_array;	

			}else{

				$this->response['code'] = 401;
				$this->response['status'] = "Failed to save reply";

			}


		}else{

				$this->response['code'] = 401;
				$this->response['status'] = "Invalid token";			
		}

	}


	//Load replies
	protected function load_posts_reply_request(){	

		if($this->auth->get($this->user_id, $this->app_id) === 1){

			$replies = $this->obj->get_posts_replies($this->post_id, 0, 20);
			$replies_return = array();
			
			if(is_array($replies) && count($replies) > 0){

				foreach ($replies as $reply) {

					$posted_time = GetTimeAgo::time_ago($reply['timestamp']);

					$new_array = array(	"reply" => $reply['Reply'], "posted" => $posted_time);

					$replies_return[] = $new_array;

				}

				$this->response = $replies_return;

			}else{
				$this->response['code'] = 403;
				$this->response['status'] = "No replies found";
			}

		}else{

			$this->response['code'] = 401;
			$this->response['status'] = "Invalid token";

		}


	}


	//Like or dislike post
	protected function like_dislike_request(){

		if($this->auth->get($this->user_id, $this->app_id) === 1){

			$result = $this->obj->likeDislikePosts($this->user_id, $this->post_id, $this->like_or_dislike);
			
			if($result === 1){

				//This is the response, I am returning posts that has been liked
				$get_replied_post = $this->obj->loadPosts(1, $this->user_id, null, null, $this->post_id);
				$post_id = $get_replied_post['Id'];
				// $replies = $this->obj->get_posts_replies($post_id, 0, 3); //Select replies
				$get_flags = $this->obj->validateLikeDislikes($this->user_id, $post_id);

				$vote_flag = array("like" => 0, "dislike" => 0);

				if($get_flags == 0){

					$vote_flag = array("like" => 0, "dislike" => 1);

				}else if($get_flags == 1){

					$vote_flag = array("like" => 1, "dislike" => 0);

				}

				$posted_time = GetTimeAgo::time_ago($get_replied_post['timestamp']);

				$new_array = array(	"post_id" => $post_id,
									"user_id" => $get_replied_post['UserId'],
									"Post" => stripslashes($get_replied_post['Post']),
									"likes_count" => $get_replied_post['likes_count'],
									"dislike_count" => $get_replied_post['dislike_count'],
									"reply_count" => $get_replied_post['reply_count'],
									"posted" => $posted_time,
									"flags" => $vote_flag
								);	

				$this->response[] = $new_array;	


			}else if($result === 0){

				$this->response['code'] = 401;
				$this->response['status'] = "Requst denied";

			}

		}else{
				$this->response['code'] = 401;
				$this->response['status'] = "Invalid token";			
		}


	}



	//Get notifications
	protected function get_notifications_request(){
		
		if($this->auth->get($this->user_id, $this->app_id) === 1){
			
			$notification = $this->obj->getNotifications($this->user_id);

			$this->response = $notification;		

		}else{

			$this->response['code'] = 401;
			$this->response['status'] = "Invalid token";

		}

	}


	//Search posts
	protected function search_posts_request(){
			
		if($this->auth->get($this->user_id, $this->app_id) === 1){
			
			$posts = $this->obj->searchPosts($this->post);
			$posts_return = array();

			if(count($posts) > 0){

				foreach ($posts as $post) {

					$posted_time = GetTimeAgo::time_ago($post['timestamp']);
					$post_id = $post['Id'];
					$get_flags = $this->obj->validateLikeDislikes($this->user_id, $post_id);

				
					if($get_flags == "none"){
						
						$vote_flag = array("like" => 0, "dislike" => 0);
				
					}else if($get_flags == 0){

						$vote_flag = array("like" => 0, "dislike" => 1);

					}else if($get_flags == 1){

						$vote_flag = array("like" => 1, "dislike" => 0);

					}

					$new_array = array(	"post_id" => $post_id,
										"user_id" => $post['UserId'],
										"Post" => stripslashes($post['Post']),
										"likes_count" => $post['likes_count'],
										"dislike_count" => $post['dislike_count'],
										"reply_count" => $post['reply_count'],
										"posted" => $posted_time,
										"flags" => $vote_flag

									);

					$posts_return[] = $new_array;

				}

				$this->response = $posts_return;

			}else{
				$this->response['code'] = 403;
				$this->response['status'] = "No post found";
			}


		}else{

				$this->response['code'] = 401;
				$this->response['status'] = "Invalid token";			
		}

	}

	//Delete post 
	protected function delete_post_request(){

		if($this->auth->get($this->user_id, $this->app_id) === 1){
			
			$delete = $this->obj->user_delete_posts($this->post_id, $this->user_id);
			$this->response['res'] = $delete;
			if($delete === 1){

				$this->response['code'] = 200;
				$this->response['status'] = "post successfully deleted";

			}else{

				$this->response['code'] = 403;
				$this->response['status'] = "Failed to delete post";

			}
		
		}else{

			$this->response['code'] = 401;
			$this->response['status'] = "Invalid token";

		}

	}

	//Update post 
	protected function edit_post_request(){

		if($this->auth->get($this->user_id, $this->app_id) === 1){
			
			$update = $this->obj->user_edit_posts($this->post_id, $this->user_id, $this->post);

			if($update === 1){

				$get_updated_post = $this->obj->loadPosts(1, $this->user_id, null, null, $this->post_id);
				$post_id = $get_updated_post['Id'];
			
				$get_flags = $this->obj->validateLikeDislikes($this->user_id, $post_id);

				$vote_flag = array("like" => 0, "dislike" => 0);

				if($get_flags == 0){

					$vote_flag = array("like" => 0, "dislike" => 1);

				}else if($get_flags == 1){

					$vote_flag = array("like" => 1, "dislike" => 0);

				}

				$posted_time = GetTimeAgo::time_ago($get_updated_post['timestamp']);

				$new_array = array(	"post_id" => $post_id,
									"user_id" => $get_updated_post['UserId'],
									"Post" => stripslashes($get_updated_post['Post']),
									"likes_count" => $get_updated_post['likes_count'],
									"dislike_count" => $get_updated_post['dislike_count'],
									"reply_count" => $get_updated_post['reply_count'],
									"posted" => $posted_time,
									"flags" => $vote_flag
								);	

				$this->response = $new_array;

			}else{

				$this->response['code'] = 403;
				$this->response['status'] = "Failed to update post";

			}
		

		}else{

			$this->response['code'] = 401;
			$this->response['status'] = "Invalid token";

		}

	}


	//Get all chatrooms
	protected function get_all_chatrooms(){

		if($this->auth->get($this->user_id, $this->app_id) === 1){
			
			$load_chatrooms = $this->obj->load_chatrooms($this->user_id);

			$this->response = $load_chatrooms;		

		}else{

			$this->response['code'] = 401;
			$this->response['status'] = "Invalid token";

		}

	}


	//Create chatroom
	protected function create_chatrooms(){

		if($this->auth->get($this->user_id, $this->app_id) === 1){
			
			$create_chatroom = $this->obj->create_chatroom($this->user_id , $this->room_title, $this->coordinates, $this->radius, $this->descr);

			if($create_chatroom === 0){

				$this->response['code'] = 402;
				$this->response['status'] = "Failed to create chatroom";

			}else if($create_chatroom === 1){

				$this->response['code'] = 200;
				$this->response['status'] = "Chatroom " . $this->room_title . " successfully created";

			}	

		}else{

			$this->response['code'] = 401;
			$this->response['status'] = "Invalid token";

		}

	}



	//User join chatroom
	protected function user_join_chatroom(){

		if($this->auth->get($this->user_id, $this->app_id) === 1){
			
			$join_chatroom = $this->obj->add_user_to_chatroom($this->user_id , $this->chatroom_id);

			if($join_chatroom === 0){

				$this->response['code'] = 402;
				$this->response['status'] = "Failed to join chatroom";

			}else if($join_chatroom === 1){

				$this->response['code'] = 200;
				$this->response['status'] = "Successfully join chatroom";

			}	

		}else{

			$this->response['code'] = 401;
			$this->response['status'] = "Invalid token";

		}

	}


	//User leave chatroom
	protected function user_leave_chatroom(){

		if($this->auth->get($this->user_id, $this->app_id) === 1){
			
			$leave_chatroom = $this->obj->delete_user_from_chatroom($this->user_id , $this->chatroom_id);

			if($leave_chatroom === 0){

				$this->response['code'] = 402;
				$this->response['status'] = "Failed to leave chatroom";

			}else if($leave_chatroom === 1){

				$this->response['code'] = 200;
				$this->response['status'] = "Successfully leave chatroom";

			}	

		}else{

			$this->response['code'] = 401;
			$this->response['status'] = "Invalid token";

		}

	}


	//Delete chatroom
	protected function delete_chatroom(){

		if($this->auth->get($this->user_id, $this->app_id) === 1){
			
			$delete_chatroom = $this->obj->delete_chatroom($this->user_id , $this->chatroom_id);

			if($delete_chatroom === 0){

				$this->response['code'] = 402;
				$this->response['status'] = "Failed to delete chatroom";

			}else if($delete_chatroom === 1){

				$this->response['code'] = 200;
				$this->response['status'] = "Successfully delete chatroom";

			}	

		}else{

			$this->response['code'] = 401;
			$this->response['status'] = "Invalid token";

		}

	}


	//Save chatroom messages
	protected function save_message(){

		if($this->auth->get($this->user_id, $this->app_id) === 1){
			
			$save_message = $this->obj->save_chatroom_message($this->user_id, $this->chatroom_id, $this->message);

			if($save_message === 0){

				$this->response['code'] = 402;
				$this->response['status'] = "Unable to send message";

			}else if($save_message === 1){

				$this->response['code'] = 200;
				$this->response['status'] = "Message sent successfully";

			}	

		}else{

			$this->response['code'] = 401;
			$this->response['status'] = "Invalid token";

		}

	}


	//Get chatroom messages
	protected function get_message(){

		if($this->auth->get($this->user_id, $this->app_id) === 1){
			
			$get_message = $this->obj->get_chatroom_message($this->user_id, $this->chatroom_id);

			if(is_array($get_message) && count($get_message) >= 1){

				$message_data = array();

				foreach ($get_message as $message) {
					
					$message_data['id'] = $message['id'];
					$message_data['chatroom_id'] = $message['room_id'];
					$message_data['message'] = $message['message_body'];
					$message_data['time'] = GetTimeAgo::formatted_time($message['date_time']);

					$this->response[] = $message_data;
				}

			}else if(is_array($get_message) && count($get_message) === 0){

				$this->response['code'] = 401;
				$this->response['status'] = "No message found";

			}else{

				$this->response['code'] = 402;
				$this->response['status'] = "Unable to get message";

			}

		}else{

			$this->response['code'] = 401;
			$this->response['status'] = "Invalid token";

		}

	}



	//Acknowledge chatroom messages
	protected function ack_message(){

		if($this->auth->get($this->user_id, $this->app_id) === 1){
			
			$ack_message = $this->obj->acknowledge_message($this->user_id, $this->chatroom_id);
			$this->response['txt'] = $ack_message;
			if($ack_message === 1){

				$this->response['code'] = 200;
				$this->response['status'] = "Message successfully acknowledged";

			}else if($ack_message === 0){

				$this->response['code'] = 402;
				$this->response['status'] = "Unable to acknowledge message";

			}

		}else{

			$this->response['code'] = 401;
			$this->response['status'] = "Invalid token";

		}

	}


	// Recover password
	protected function recover_password_request(){

		$checkuser  = $this->obj->checkUserExists_recoverpass($this->email);

		$now_time	= time();	

		if(count($checkuser) >= 1){

			$user_id = $checkuser['Id'];

			if($checkuser['count_id'] == 1){

				$recover = $this->obj->recover_password($user_id, $now_time);
				
				if($recover === 0){

					$this->response['code'] = 402; 
					$this->response['status'] = "Failed to send email"; 

				}else if($recover === 1){

					$send_email = $this->obj->send_email($this->email, $user_id); //Send email

					if($send_email){

						$this->response['code'] = 200; 
						$this->response['status'] = "The link has successfully send to your email, please access your email to confirm and change your password."; 
					
					}else{

						$this->response['code'] = 402; 
						$this->response['status'] = "Failed to send email";							

					}

				}else{

					$this->response['code'] = 401; 
					$this->response['status'] = "Request failed"; 
				}
				

			} else if($checkuser['count_id'] === 0){

				$this->response['code'] = 205; 
				$this->response['status'] = "Email does not exists";

			}

		}else{
			$this->response['code'] = 205; 
			$this->response['status'] = "Email does not exists "; 
		}


		
	}


	protected function contactUs(){

		if($this->obj->contact_us($this->email, $this->message)){

			$this->response['code'] = 200;
		    $this->response['status'] = "Message successfully send";
		}else{

			$this->response['code'] = 401;
			$this->response['status'] = "Message Failed to send system problem";
		}
		

	}
    

	//Show error
	protected function error(){

		$this->response['code'] = 404;
		$this->response['status'] = "Not found"; 

	}


	//return response
	public function displayResponse(){

		return json_encode($this->response);

	}

}