<?php

//Get time ago
class GetTimeAgo{


    public static function time_ago($datetime){

            //type cast, current time, difference in timestamps
            $timestamp      = strtotime($datetime);
            $current_time   = time();
            $diff           = $current_time - $timestamp;
    
            $formated_date = date('d-m-Y', $timestamp);
            $formatted_time = date('G:i:s', $timestamp);
            $formatted_time = date('g:ia', strtotime($formatted_time)); 
            $returned_time = "";

            //intervals in seconds
            $intervals = array (
                'day' => 86400, 
                'hour' => 3600, 
                'minute' => 60
            );
           
            //Check if post is less than a minute
            if ($diff < 60){

                $returned_time = "moments ago";
            }       

            //Check if post is older than a minute but younger than the hour
            if ($diff >= 60 && $diff < $intervals['hour']) {

                $diff = floor($diff / $intervals['minute']);
                $returned_time = $diff == 1 ? $diff . ' minute ago' : $diff . ' minutes ago';
            }       

            //Check if post is older that an hour but younger that the day
            if ($diff >= $intervals['hour'] && $diff < $intervals['day']){

                $diff = floor($diff / $intervals['hour']);

                if($diff == 1 && self::date_diff($formated_date) == 0){

                    $returned_time = $diff . ' hour ago';

                }elseif ($diff > 1 && self::date_diff($formated_date) == 0) {

                    $returned_time = $diff . ' hours ago';

                }elseif ($diff > 1 && self::date_diff($formated_date) == 1) {

                    $returned_time = "Yesterday at " . $formatted_time;

                }elseif ($diff > 1 && self::date_diff($formated_date) > 1) {

                    $returned_time = self::format_date($formated_date) . " at " . $formatted_time;

                }

            }   

            //Check if time is older than the day
            if ($diff >= $intervals['day']) {

                $diff = floor($diff / $intervals['day']);

                if($diff == 1){
                    $returned_time = "Yesterday at " . $formatted_time;
                }else if($diff > 1){
                    $returned_time = self::format_date($formated_date) . " at " . $formatted_time;
                }
            }    

            return $returned_time;
 

        }



        //Format date
        private static function format_date($date){

            $dateList = explode("-", $date);
            $monthList = array("01" => "Jan", "02" => "Feb", "03" => "Mar", "04" => "Apr", "05" => "May", "06" => "Jun", "07" => "Jul", "08" => "Aug", "09" => "Sep", "10" => "Oct", "11" => "Nov", "12" => "Dec");
            
            $dateDay = $dateList[0];
            $dateMon = $dateList[1];
            $dateYear = $dateList[2];

            return $dateDay . " " . $monthList[$dateMon] . " " . $dateYear;


        }


        //Get different between 2 dates
        private static function date_diff($date){

             $d = explode("-", $date);

             $final_date = $d[2] . "-" . $d[1] . "-" . $d[0]; //Reform date

             $first_date = time();
             $second_date = strtotime($final_date);
             $datediff = $first_date - $second_date;

             return floor($datediff / (60 * 60 * 24));

        }


}

