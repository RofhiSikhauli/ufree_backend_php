<?php


final class UserController{
	
	private $link;

	public function __construct(){

		$this->link = new DBQueries();

	}
	
	//Check if user exists
	private function checkUserExists($email){

	 	$stmt = $this->link->select(array("Email"))->from("users")->where("Email = :email")->row_count(array(":email" => $email));
	 	 
	 	return $stmt;
	}


	//Check if user exists
	public function checkUserExists_recoverpass($email){

	 	$stmt = $this->link->select(array("Email", "Id", "count(Id) as count_id"))->from("users")->where("Email = :email")->fetch(array(":email" => $email));
	 	 
	 	return $stmt;
	}


    //Check if user id exists in a recovery table
	private function checkidExists($user_id){

	 	$stmt = $this->link->select(array("user_id","expiry_time"))->from("password_recovery")->where("user_id = :user_id")->fetch(array(":user_id" => $user_id));
	 	 
	 	return $stmt;
	}

	//Generate salt
	private function generateSalt($email){

		$time = time();
		return hash('sha512', $time . $email);

	}


	//register new user
	public function create_new_user($email, $password){

		$result = 0;
		$salt = $this->generateSalt($email);
		$password = hash('sha512', $password . $salt);

		if($this->checkUserExists($email) === 0){

			$result = $this->link->insert("users")->columns(array("Email", "salt", "password"))->values(array(":email", ":salt", ":pass"))->run(array(":email" => $email, ":salt" => $salt, ":pass" => $password));
		
		}else if($this->checkUserExists($email) === 1){

			$result = 2;

		}

		return $result;

	}


	//Get the salt from table
	public function get_salt($email){

	 	$stmt = $this->link->select(array('salt'))->from("users")->where("Email=:email")->fetch(array(":email" => $email));
	 	return $stmt['salt'];

	}

	//Login user
	public function loginUser($email, $password){
		
	    $result = $this->link->select(array("Id", "Password"))->from("users")->where("Email=:email && status = 1")->fetch(array(":email" => $email));

		return $result; 	

	}

	//Delete user
	public function deleteUser($id){

		$result = $this->link->update("users")->set(array("status" => 0))->where("Id = :id")->run(array(':id' => $id ));

		return $result;
	}

	// Recover password
	public function recover_password($user_id, $now_time){

		$result = 0;
		$token = $this->generateSalt($user_id);
		$fields = array("user_id" , "token", "expiry_time");
		$values = array(":user_id", ":token", ":expiry_time");
		$field_values = array(":user_id" => $user_id, ":token" => $token, ":expiry_time" => $now_time);
		$result = $this->link->insert("password_recovery")->columns($fields)->values($values)->run($field_values);
		//Email information
	
		return $result;


	}


	//Send email
	public function send_email($email, $user_id){

		$token		 =  $this->generateSalt($user_id);
		$subject	 = "Recover password";
		$url	 	 = "http://www.ufree.co.za/?id=" . $user_id . "&token=" . $token . "&email=" . $email; 

		$message 	 = "You request to change your password, please click <a href='" . $url . "'>here</a> to change your password\n\n";

		$message 	 .= $url;

		$header 	 = "From: no-reply@ufree.co.za";

        return mail($email, $subject, $message, $header);



	}




	// Check if link expired
	public function expiredLink($expired_date, $user_id){

	}
	// Recover password
	public function confirm_password($email,$password){

		$result = $this->link->update("users")->set(array("password" => ":password" ))->where("Id = :id")->run(array(':id' => $id, ":password" => $password ));

		return $result;
	}

		 
}
