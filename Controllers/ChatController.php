<?php


final class ChatController{

	private $link, $auth;

	public function __construct(){

		$this->link = new DBQueries();

	}


	// Get chatrooms lists
	public function load_chatrooms($user_id){

		$cols = array("id", "title", "location", "radius", "description", "users_count", "date_time");
		$where_clause = "user_id != :user_id";
		$vals = array(":user_id" => $user_id);

		$stmt = $this->link->select($cols)->from("chatrooms")->where($where_clause)->limit(0, 20)->fetch_all($vals);

		return $stmt;

	}


	//Search chatrooms
	public function search_chatrooms($user_id, $que){

		$cols = array("id", "title", "location", "radius", "description", "users_count", "date_time");
		$where_clause = "user_id != :user_id && title like concat('%', :q, '%')";
		$vals = array(":user_id" => $user_id, ":q" => $que);

		$stmt = $this->link->select($cols)->from("chatrooms")->where($where_clause)->limit(0, 20)->fetch_all($vals);

		return $stmt;

	}


	// Create chatroom
	public function create_chatroom($user_id , $title, $location, $radius, $descr){

		$result = 0;
		$timestamp = date("Y-m-d H:i:s");
		$cols = array("user_id", "title", "location", "radius", "description", "date_time");
		$vals = array(":user_id", ":title", ":location", ":radius", ":description", ":date_time");
		$runVals = array(":user_id" => $user_id, ":title" => $title, ":location" => $location, ":radius" => $radius, ":description" => $descr, ":date_time" => $timestamp);

	 	$stmt = $this->link->insert("chatrooms")->columns($cols)->values($vals)->run($runVals); //Insert chatroom
	 	
	 	if($stmt === 1){

	 		$result = $this->add_user_to_chatroom($user_id, $this->link->getLastId());

	 	}

	 	return $result;

	}


	// Add user to a chatroom
	public function add_user_to_chatroom($user_id, $chatroom_id){

		$time = time();
		$cols = array("user_id", "chatroom_id", "seen");
		$vals = array(":user_id", ":chatroom_id", ":seen");
		$runVals = array(":user_id" => $user_id, ":chatroom_id" => $chatroom_id, ":seen" => $time);

		$stmt = $this->link->insert("chatroom_users")->columns($cols)->values($vals)->run($runVals);

	 	return $stmt;

	}


	// Remove chatroom
	public function delete_chatroom($user_id, $chatroom_id){

		$where_clause = "id = :chatroom_id && user_id = :user_id";
		$runVals = array(":user_id" => $user_id, ":chatroom_id" => $chatroom_id);

		$stmt = $this->link->delete("chatrooms")->where($where_clause)->run($runVals);

	 	return $stmt;

	}


	// Remove user from chatroom
	public function delete_user_from_chatroom($user_id, $chatroom_id){

		$where_clause = "user_id = :user_id && chatroom_id = :chatroom_id";
		$runVals = array(":user_id" => $user_id, ":chatroom_id" => $chatroom_id);

		$stmt = $this->link->delete("chatroom_users")->where($where_clause)->run($runVals);

	 	return $stmt;

	}


	//Save message
	public function save_chatroom_message($user_id, $chatroom_id, $message){

		$date_time = date("Y-m-d H:i:s");
		$cols = array("user_id", "room_id", "message_body", "date_time");
		$vals = array(":user_id", ":room_id", ":message", ":date_time");
		$runVals = array(":user_id" => $user_id, ":room_id" => $chatroom_id, ":message" => $message, ":date_time" => $date_time);

		$stmt = $this->link->insert("messages")->columns($cols)->values($vals)->run($runVals);

	 	return $stmt;

	}


	//Get message
	public function get_chatroom_message($user_id, $chatroom_id){

		$cols = array("id", "room_id", "message_body", "date_time");
		$where_clause = "user_id != :user_id && status = 0";
		$vals = array("user_id" => $user_id);

		$stmt = $this->link->select($cols)->from("messages")->where($where_clause)->fetch_all($vals);

		return $stmt;

	}


	//Acknowledge/delete read messages
	public function acknowledge_message($user_id, $chatroom_id){

		$where_clause = "user_id = :user_id && room_id = :room_id";
		$runVals = array(":user_id" => $user_id, ":room_id" => $chatroom_id);

		$stmt = $this->link->delete("messages")->where($where_clause)->run($runVals);

	 	return $stmt;

	}

}