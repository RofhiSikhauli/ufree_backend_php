<?php


class Requests{

	protected $obj, $url, $request_type, $param1, $param2, $email, 
			  $password, $app_id, $app_version, 
			  $sel_start, $sel_limit, $post, $coordinates, 
                          $seen_by, $post_type, $room_title, $radius, $message;

	public function __construct(){

        $this->url = &$_GET['url'];
        $this->url = explode("/", $this->url);
        $this->param1 = isset($this->url[0]) ? $this->url[0] : exit("Path not found");
        $this->param2 = isset($this->url[1]) ? $this->url[1] : exit("Path not found");

        $this->request_type = ($this->param2 === "LoadPosts" || $this->param2 === "Notifications") ? $_GET : $_POST;
        
        $this->email = isset($this->request_type['email']) ? $this->validate($this->request_type['email']) : NULL;
        $this->password = isset($this->request_type['password']) ? $this->validate($this->request_type['password']) : NULL;
        $this->app_id = isset($this->request_type['app_id']) ? $this->validate($this->request_type['app_id']) : NULL;
        $this->user_id = isset($this->request_type['user_id']) ? $this->validate($this->request_type['user_id']) : NULL;
        $this->app_version = isset($this->request_type['app_version']) ? $this->validate($this->request_type['app_version']) : NULL;
       
        $this->sel_start = isset($this->request_type['sel_start']) ? $this->request_type['sel_start'] : NULL;
        $this->sel_limit = isset($this->request_type['sel_limit']) ? $this->request_type['sel_limit'] : NULL;

        $this->post = isset($this->request_type['post']) ? $this->validate($this->request_type['post']) : NULL;
        $this->coordinates = isset($this->request_type['coords']) ? $this->request_type['coords'] : NULL;
        $this->seen_by = isset($this->request_type['seen_by']) ? $this->validate($this->request_type['seen_by']) : NULL;

        $this->post_id = isset($this->request_type['post_id']) ? $this->validate($this->request_type['post_id']) : NULL;
        $this->reply_post = isset($this->request_type['reply']) ? $this->validate($this->request_type['reply']) : NULL;

        $this->like_or_dislike = isset($this->request_type['type']) ? $this->validate($this->request_type['type']) : NULL;
        
        $this->post_type = isset($this->request_type['post_type']) ? $this->validate($this->request_type['post_type']) : NULL;

        $this->room_title = isset($this->request_type['title']) ? $this->validate($this->request_type['title']) : NULL;
        $this->radius = isset($this->request_type['radius']) ? $this->validate($this->request_type['radius']) : NULL;
        $this->descr = isset($this->request_type['descr']) ? $this->validate($this->request_type['descr']) : NULL;
        $this->chatroom_id = isset($this->request_type['chatroom_id']) ? $this->validate($this->request_type['chatroom_id']) : NULL;
        $this->message = isset($this->request_type['message']) ? $this->validate($this->request_type['message']) : NULL;

        $class_name = $this->param1 . "Controller";
        
        if(class_exists($class_name)){
              $this->obj = new $class_name();  //instantiate class
        }

        $this->get_requests();

	}

	private function get_requests(){

		switch($this->param2){

        	case "Login":

        		$this->login_request();

        		break;

        	case "Register":

        		$this->register_request();

        		break;

        	case "LoadPosts":

        		$this->load_posts_request();

        		break;

            case "SavePosts":

                $this->update_post_request();

                break;

            case "DeletePost":

                $this->delete_post_request();

                break;

        	case "EditPost":

        		$this->edit_post_request();

        		break;

            case "SaveReply":

                $this->post_reply_request();

                break;               

        	case "LoadReply":

        		$this->load_posts_reply_request();

        		break;

        	case "LikeDislike":

        		$this->like_dislike_request();

        		break;

        	case "Notifications":

        		$this->get_notifications_request();

        		break;
                        
                case "SearchPosts":

                        $this->search_posts_request();

                        break;

                case "LoadChatrooms":

                        $this->get_all_chatrooms();

                        break;

                default:

                case "AddChatrooms":

                        $this->create_chatrooms();

                        break;

                default: 

                case "JoinChatrooms":

                        $this->user_join_chatroom();

                        break;

                default: 

                case "DeleteChatrooms":

                        $this->delete_chatroom();

                        break;

                default: 

                case "LeaveChatrooms":

                        $this->user_leave_chatroom();

                        break;

                default: 

                case "SaveMessage":

                        $this->save_message();

                        break;

                default:

                case "GetMessage":

                        $this->get_message();

                        break;

                default:

                case "AckMessage":

                        $this->ack_message();

                        break;
                default:

                case "RecoverPassword":

                        $this->recover_password_request();

                        break;
                default:
                
                case "ContactUs":

                        $this->contactUs();

                        break;

        	default: 
        	
        		$this->error();

        		break;


        }

	}

	//Validate fields
	private function validate($field){

		return addslashes($field);

	}



}